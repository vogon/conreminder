Con Reminder
============

a friendly reminder Twitter bot by:

@TrinAndTonic, @GeekyLyndsay, @legobutts, @frequentbeef (advice)

@vogon (code)

trying to make your convention experience a pleasant one.

Care and feeding of conreminder.rb
----------------------------------
information about shows goes in data/shows.json

advice goes one-advice-per-line in data/reminders.txt

run rake dry_run to test it out

run rake tweet to tweet