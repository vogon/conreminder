Sit down for ten consecutive minutes.
Did you eat today? You should eat at least three times today. 
Santize yourself. Yes, again. 
Did you touch someone today? You should not have done that actually
You should drink 2 liters of water on a normal day. This is not a normal day. Go for 3.
Be considerate. Be EVEN M O R E C O N S I D E R A T E
When do you need to be awake tomorrow? Go to sleep before then. 
Costumes aren't consent. Costumes aren't consent. Costumes still aren't consent. 
Did you pack your medicine? 
Is your phone charged? 
Have you checked in with your significant other at home? Tell them you're alive. 
What was the last vegetable you ate?
It is okay to miss the party. Self-care comes first. 
Go somewhere quiet for 15 minutes. 
Deep. Breaths. 
That person who was rude to you might be having a bad day. Don't escalate.
If you drink coffee, take some time to enjoy a cup. 
You can survive this. We believe in you. 
That volunteer doesn't control the convention. Don't take it out on them. 
3 hours of sleep, 2 meals, 1 shower. Every day at the very least. 
Who hasn't taken a break from the booth yet? Go kick them out. 
SANITIZE 
Deoderant is great! But strong scents can overwhelm in a crowd. Choose wisely.
You can fit a travel toothbrush and toothpaste in your bag. Just sayin'.
You can never pack too many pairs of underpants. 
Take a moment to stretch your hamstrings. 
Do you REALLY need to carry your laptop on the show floor? 
Did you stop at the bank to get your float? 
It's loud, but keep your distance in conversations. That's how disease spreads!
Why shake hands when a fist bump does the trick? 
friendly reminder: Socially Awkward Isn't An Excuse - http://www.doctornerdlove.com/2014/03/socially-awkward-isnt-an-excuse/ @DrNerdLove
Let's Not Make This Weird - https://www.youtube.com/watch?v=XsHLHn8nVDM @JennAndTrin
Do you love their tweets? Still ask before you go in for a hug. 
The locals aren't attending the con. They just want to get to work. Be kind.
Ask before you snap a photo. 
Media guests are people too. 
Don't catch up with friends in the middle of the hallway. Move to the side. 
Visiting your friend's booth is fun! Staying for more than ten minutes to talk is not!
Cosplayers love credit! Ask for details if you plan to post a pic on your blog.
Could you make that? Don't tell the person who's trying to sell it right now. 
It's okay to go back to your hotel and scream into a pillow for a while. 
You can't spend quality time with everyone on your list. It's ok. They know. 
Did you see a celebrity in the restroom? DON'T TALK TO THEM. 
Be understanding of friends who need a break from being around other humans.
Poop regularly. Have some warm liquids, yogurt, and fiber. End your day with tea.
Don't follow the celebrities around if you're not friends. It's threatening, not cute.
Your friends don't have to all get along. 

It's okay to turn off your phone. 
Shut up for a minute. 
Comfortable, Cute Shoes > Comfortable Shoes > Cute Shoes
Creators deserve to be paid for their work. This is not the time to haggle. 
Asking a female attendee of a gaming convention if she even plays games is very rude. 
Asking a female attendee of a comics convention if she even reads comics is very rude.
Unsure of somebody's gender? Consider minding your own fucking business. 
Convention center food is typically unhealthy and overpriced. Consider packing your own.
Breaks are necessary to be your best. You can't do quality work without them. 
Nobody has ever regretted packing too many socks. 
Body powder is the unsung hero of convention life. Stay dry and happy! 
Every morning, do this check: Phone, Keys, Wallet, Water, Badge. 
Tampons. 
"You did an amazing job on your costume!" is an appropriate cosplay compliment.
People will like things that you don't like. And that's okay. 
Sometimes, the panel will fill up before you get there. You will live. 
Be considerate of other people's time. If you're going to be late for an appointment, let them know.
Some helpful tips from Bleeding Cool: http://www.bleedingcool.com/2014/04/08/essential-8-tips-to-having-the-most-fun-youve-ever-had-at-a-comic-con/
Cons are about having fun. Remember to let yourself have fun. Let everyone else have fun, too.