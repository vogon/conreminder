require 'json'
require 'twitter'
require 'twitter-text'

module ConReminder
	def self.reminders
		reminders = File.read('data/reminders.txt')

		reminders = reminders.lines.
			map { |s| s.strip }.
			select { |s| s != "" }

		reminders.each do |reminder|
			if Twitter::Validation.tweet_invalid?(reminder)
				fail "invalid tweet: \'#{reminder}\'"
			end
		end

		return reminders
	end

	def self.shows
		json = File.read('data/shows.json')
		shows = JSON.parse(json)

		return shows
	end

	def self.current_shows(time)
		current_shows = []

		self.shows.each do |show|
			show["times"].each do |block|
				begin_time = DateTime.parse(block["start"])
				end_time = DateTime.parse(block["end"])

				# puts "time is #{time}"
				# puts "#{show["human_name"]} runs from #{begin_time} to #{end_time}..."

				if time > begin_time && time < end_time
					# show's running right now
					current_shows << show
				end
			end
		end

		return current_shows
	end

	def self.tweet(options = {})
		if !options[:dry_run]
			client = Twitter::REST::Client.new do |config|
				config.consumer_key = API_KEY
				config.consumer_secret = API_SECRET
				config.access_token = ACCESS_TOKEN
				config.access_token_secret = ACCESS_TOKEN_SECRET
			end
		end

		# rebrand ourselves based on current shows
		shows = nil

		if options[:dry_run]
			shows = current_shows(options[:dry_run_time])
		else
			shows = current_shows(DateTime.now)
		end

		name = "Con Reminders"
		description = "Delivering hourly reminders to convention attendees. "
		hashtag = nil

		if shows.length >= 2
			show_names = shows.map { |show| show["human_name"] }.join(", ")

			description += "Currently: #{show_names}."
			hashtag = shows.map { |show| show["hashtag"] }.shuffle[0]
		elsif shows.length == 1
			name = "#{shows[0]["acronym"]} Reminders"
			description += "Currently: #{shows[0]["human_name"]}."
			hashtag = shows[0]["hashtag"]
		else
			description += "Currently dormant."
		end

		if options[:dry_run]
			puts "would change name to \"#{name}\""
			puts "would change description to \"#{description}\""
		else
			client.update_profile({
				:name => name,
				:description => description
			})
		end

		# if no shows are going on, don't tweet anything
		return if shows.length == 0

		# tweet a random reminder different from the last one
		if options[:dry_run]
			next_reminder = (reminders.shuffle)[0]

			# add a hashtag for #ultimate #engagement if possible
			if hashtag
				hashtagged_reminder = "#{next_reminder} #{hashtag}"

				if !Twitter::Validation::tweet_invalid?(hashtagged_reminder)
					next_reminder = hashtagged_reminder
				end
			end

			puts "would tweet \"#{next_reminder}\""
		else
			last_reminder = client.user_timeline[0].text if (client.user_timeline.length > 0)
			next_reminder = nil

			loop do
				next_reminder = (reminders.shuffle)[0]
				break if next_reminder != last_reminder
			end

			# add a hashtag for #ultimate #engagement if possible
			if hashtag
				hashtagged_reminder = "#{next_reminder} #{hashtag}"

				if !Twitter::Validation::tweet_invalid?(hashtagged_reminder)
					next_reminder = hashtagged_reminder
				end
			end

			client.update(next_reminder)
		end
	end
end